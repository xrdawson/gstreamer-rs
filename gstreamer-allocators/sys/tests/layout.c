// Generated by gir (https://github.com/gtk-rs/gir @ 74b6e47217b7)
// from gir-files (https://github.com/gtk-rs/gir-files @ 9e945716ad4c)
// from gst-gir-files (https://gitlab.freedesktop.org/gstreamer/gir-files-rs.git @ d031d210fe4e)
// DO NOT EDIT

#include "manual.h"
#include <stdalign.h>
#include <stdio.h>

int main() {
    printf("%s;%zu;%zu\n", "GstDmaBufAllocator", sizeof(GstDmaBufAllocator), alignof(GstDmaBufAllocator));
    printf("%s;%zu;%zu\n", "GstDmaBufAllocatorClass", sizeof(GstDmaBufAllocatorClass), alignof(GstDmaBufAllocatorClass));
    printf("%s;%zu;%zu\n", "GstFdAllocator", sizeof(GstFdAllocator), alignof(GstFdAllocator));
    printf("%s;%zu;%zu\n", "GstFdAllocatorClass", sizeof(GstFdAllocatorClass), alignof(GstFdAllocatorClass));
    printf("%s;%zu;%zu\n", "GstFdMemoryFlags", sizeof(GstFdMemoryFlags), alignof(GstFdMemoryFlags));
    printf("%s;%zu;%zu\n", "GstPhysMemoryAllocatorInterface", sizeof(GstPhysMemoryAllocatorInterface), alignof(GstPhysMemoryAllocatorInterface));
    return 0;
}
