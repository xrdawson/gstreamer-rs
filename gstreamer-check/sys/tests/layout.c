// Generated by gir (https://github.com/gtk-rs/gir @ 74b6e47217b7)
// from gir-files (https://github.com/gtk-rs/gir-files @ 9e945716ad4c)
// from gst-gir-files (https://gitlab.freedesktop.org/gstreamer/gir-files-rs.git @ d031d210fe4e)
// DO NOT EDIT

#include "manual.h"
#include <stdalign.h>
#include <stdio.h>

int main() {
    printf("%s;%zu;%zu\n", "GstHarness", sizeof(GstHarness), alignof(GstHarness));
    printf("%s;%zu;%zu\n", "GstTestClock", sizeof(GstTestClock), alignof(GstTestClock));
    printf("%s;%zu;%zu\n", "GstTestClockClass", sizeof(GstTestClockClass), alignof(GstTestClockClass));
    return 0;
}
